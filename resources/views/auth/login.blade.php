<!DOCTYPE html>
<html>
<head>
@extends('layouts.appHeadST')
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="{{ url('/') }}">Sistema de ventas <b> AG</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Proceso de Autenticado Para Inicio de Sesión</p>

    <form action="" method="post">
      @csrf
      <div class="form-group has-feedback">
        <input id="email" type="email" class="form-control" placeholder="Correo Electronico" name="email" value="{{ old('email') }}" required autofocus>
        @if ($errors->has('email'))
        <span class="form-group has-feedback" role="alert">
            <strong>{{ $errors->first('email') }}</strong>
        </span>
        @endif
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input id="password" type="password" class="form-control" placeholder="Contraseña" name="password" required>
        @if ($errors->has('password'))
        <span class="form-group has-feedback" role="alert">
            <strong>{{ $errors->first('password') }}</strong>
        </span>
        @endif
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <!-- /.col -->
        <div class="col-xs-6">
          <button type="submit" class="btn btn-primary btn-block btn-flat">{{ __('Entrar al Sistema') }}</button>
        </div>
        <!-- /.col -->
      </div>
      <div class="row">
        <!-- /.col -->
        <div class="col-xs-8">
          @if (Route::has('password.request'))
            <a href="{{ route('password.request') }}">{{ __('Olvidaste la Contraseña?') }}</a>
          @endif
          <a href="{{ route('register') }}" class="text-center">Registrar un Nuevo Usuario</a>
        </div>
        <!-- /.col -->
      </div>
    </form>
  
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

@extends('layouts.appFooterST')

</body>
</html>
