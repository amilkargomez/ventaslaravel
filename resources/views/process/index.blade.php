@extends('home')

@section('contentProcess')
<div class="box box-success">
  <div class="box-header with-border">
    <h3 class="box-title"><a href="/procesos/">Lista de Procesos</a></h3>
  </div>
    <div class="box-body">
    <div class="box">
            <div class="box-header">
              <h3 class="box-title">Procesos</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-striped">
                <tbody><tr>
                  <th style="width: 10px">#</th>
                  <th>Lista</th>
                  <th>Acción</th>
                </tr>
                @foreach($processIndexs as $processIndex)
                <tr>
                  <td>{{ $processIndex->idProcesses }}</td>
                  <td><a href="/procesos/{{ $processIndex->idProcesses }}">{{ $processIndex->nameProcesses }}</a></td>
                  <td><a href="/procesos/{{ $processIndex->idProcesses }}/edit">Editar</a></td>
                  <td><a href="/procesos/{{ $processIndex->idProcesses }}/confirmDelete">Borrar</a></td>
                  <td>
                @endforeach
                </tr>
              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>
    </div>
</div>
@endsection