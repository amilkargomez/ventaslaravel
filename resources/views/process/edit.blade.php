@extends('home')

@section('contenEdit')
<div class="box box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Editar Proceso {{ $processEdit->idProcesses }} - <a href="/procesos/">Lista de Procesos</a></h3>
  </div>
    <div class="box-body">
      <form action="/procesos/{{ $processEdit->idProcesses }}" method="POST">
        @csrf
        @method('put')
        <input class="form-control" type="text" placeholder="{{ $processEdit->nameProcesses }}" name="nameProcesses" value="{{ old('nameProcesses') }}">
        <input class="form-control" type="text" placeholder="{{ $processEdit->descriptionProcesses }}" name="descriptionProcesses" value="{{ old('descriptionProcesses') }}">
        <div class="box-footer">
          <button type="submit" class="btn btn-primary">Guardar</button>
        </div>
      </form>
    </div>
</div>
@endsection