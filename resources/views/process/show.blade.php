@extends('home')

@section('contenShow')
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">Detalles Proceso {{ $processShow->idProcesses }} - <a href="/procesos/">Lista de Procesos</a></h3> 
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-condensed">
                <tbody><tr>
                  <th style="width: 10px">#</th>
                  <th>Nombre</th>
                  <th>Descripción</th>
                  <th>Estado</th>
                  <th>Usuario Creador</th>
                  <th>Fecha de Creación</th>
                  <th>Fecha de Actualización</th>    
                </tr>
                <tr>
                  <td>{{ $processShow->idProcesses }}</td>
                  <td>{{ $processShow->nameProcesses }}</td>
                  <td><span class="badge bg-red">{{ $processShow->descriptionProcesses }}</span></td>
                  <td>{{ $processShow->stateProcesses }}</td>
                  <td>{{ $userShow->name }}</td>
                  <td>{{ $processShow->created_at }}</td>
                  <td>{{ $processShow->updated_at }}</td>
                </tr>
              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>
@endsection