@extends('home')

@section('content')
<div class="box box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Crear Procesos - <a href="/procesos/">Lista de Procesos</a></h3>
  </div>
    <div class="box-body">
      <form action="/procesos" method="POST">
        @csrf
        <input class="form-control" type="text" placeholder="Nombre Proceso" name="nameProcesses" value="{{ old('nameProcesses') }}">
        <input class="form-control" type="text" placeholder="Descripción" name="descriptionProcesses" value="{{ old('descriptionProcesses') }}">
        <input class="form-control" type="hidden" placeholder="Descripción" name="idUsers" value="{{ Auth::user()->idUsers }}">
        <div class="box-footer">
          <button type="submit" class="btn btn-primary">Guardar</button>
        </div>
      </form>
      @if($errors->any())
        <div class="alert alert-danger">
          <ul>
            @foreach($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
    </div>
</div>
@endsection