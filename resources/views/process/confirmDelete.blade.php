@extends('home')

@section('contentDelete')
 <div class="box-body">
    <h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">
    Borrar Proceso - {{ $processDelete->nameProcesses }}
    </h4>
    <div class="media">
        <div class="media-body">
            <div class="clearfix">
                <p class="pull-right">
                <form action="/procesos/{{ $processDelete->idProcesses }}" method="POST">
                    @csrf
                    @method('delete')
                    <button type="submit" class="btn btn-block btn-danger btn-flat">Borrar</button>           
                </form>
                <button type="submit" class="btn btn-block btn-success btn-flat"><a href="/procesos">Atras</a></button>
                </p>
                </div>
            </div>
        </div>
    </div>
@endsection