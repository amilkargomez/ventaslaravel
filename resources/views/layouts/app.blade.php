<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  @extends('layouts.appHeadST')
</head>
<body class="hold-transition lockscreen">
<!-- Automatic element centering -->
<div class="lockscreen-wrapper">
  <div class="lockscreen-logo">
    <a href="{{ url('/') }}">{{ config('app.name', 'Sistema de Ventas') }} - Ventas <b>AG</b></a>
  </div>
  <!-- START LOCK SCREEN ITEM -->
  <div class="text-center">
  @guest
    <a href="{{ route('login') }}">Entrada al Sistema Con Mail y Contraseña</a>
  </div>
  @if (Route::has('register'))
  <div class="text-center">
    <a href="{{ route('register') }}">Crear Un Nuevo Usuario</a>
  </div>
  @endif
@else
  <div class="text-center">
    <a href="/home"><span><b>{{ Auth::user()->name }}</b></span></a>
  </div>
  <div class="text-center">
  <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
    <span><b>{{ __('Salir del Sistema') }}</b></span>
  </a>
  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
  @csrf
  </div>
  @endguest
  <!-- /.lockscreen-item -->
  <div class="help-block text-center">
    Entrada al Sistema de Ventas Amilcar Gomez - Laravel
  </div>
  <div class="lockscreen-footer text-center">
    Copyright &copy; 2019 <b><a href="https://twitter.com/amilkargomezc?lang=es" class="text-black">Amilcar Gómez</a></b><br>
    All rights reserved
  </div>
</div>

<main>
  @yield('content')
</main>

@extends('layouts.appFooterST')
</body>
</html>
