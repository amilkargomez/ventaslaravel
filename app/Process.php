<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Process extends Model
{

    protected $primaryKey = 'idProcesses';

    public function user() {
        return $this->belongsTo(User::class);
    }
}