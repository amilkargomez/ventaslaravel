<?php

namespace App\Http\Controllers;

use App\Process;
use App\User;
use Illuminate\Http\Request;

class ProcessController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('process.index', [
            'processIndexs' => Process::all()
        ]);   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Process $process)
    {
        return view('process.create', [
            'process' => $process
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $process = new Process();
        $process->nameProcesses = $request->get('nameProcesses');
        $process->descriptionProcesses = $request->get('descriptionProcesses');
        $process->stateProcesses = 1;
        $process->idUsers = $request->get('idUsers');
        $process->save();
        return redirect('/procesos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Process $proceso)
    {   
        //dd($process);
        //$process = Process::findOrFail($process);
        $user = User::findOrFail($proceso['idUsers']);
        return view('process.show', [
            'processShow' => $proceso,
            'userShow' => $user,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Process  $process
     * @return \Illuminate\Http\Response
     */
    public function edit($idProcesses)
    {
        $processEdit = Process::findOrFail($idProcesses);
        return view('process.edit', [
            'processEdit' => $processEdit
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Process  $process
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idProcesses)
    {
        $processEdit = Process::find($idProcesses);
        $processEdit->nameProcesses = $request->get('nameProcesses');
        $processEdit->descriptionProcesses = $request->get('descriptionProcesses');
        $processEdit->save();
        return redirect('/procesos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Process  $process
     * @return \Illuminate\Http\Response
     */
    public function destroy(Process $process)
    {
        $report = Process::find($process);
        $report->delete();
        return redirect('/procesos');
    }

    public function confirmDelete($id) {
        $report = Process::find($id);
        return view('process.confirmDelete', [
            'processDelete' => $report
        ]);
    }
    
}
